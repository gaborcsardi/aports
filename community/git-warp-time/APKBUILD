# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=git-warp-time
pkgver=0.5.0
pkgrel=0
pkgdesc="Reset timestamps of Git repository files to the time of the last modifying commit"
url="https://github.com/alerque/git-warp-time"
license="GPL-3.0-only"
arch="all !s390x !riscv64" # blocked by rust/cargo
makedepends="cargo cargo-auditable"
subpackages="$pkgname-bash-completion $pkgname-fish-completion $pkgname-zsh-completion"
source="https://github.com/alerque/git-warp-time/archive/v$pkgver/git-warp-time-$pkgver.tar.gz"


prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/$pkgname -t "$pkgdir"/usr/bin/

	# shell completions
	find target/release -name $pkgname.bash \
		-exec install -Dm644 {} "$pkgdir"/usr/share/bash-completion/completions/$pkgname \;
	find target/release -name $pkgname.fish \
		-exec install -Dm644 {} "$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish \;
	find target/release -name _$pkgname \
		-exec install -Dm644 {} "$pkgdir"/usr/share/zsh/site-functions/_$pkgname \;
}

sha512sums="
720bfa45c80a505d959585aa5fcd8c685e4137715c7cf826d34fd2b0526ca39271858a51f36bb8396c0a4632a4a06f10b76b33362da51cbddccc0bdcaaaf506e  git-warp-time-0.5.0.tar.gz
"
