# Contributor: Chloe Kudryavtsev <code@toast.bunkerlabs.net>
# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=minio-client
_pkgname=mc
pkgver=0.20230922.050746
# 0.20230323.200304 -> 2023-03-23T20-03-04Z
_pkgver="${pkgver:2:4}-${pkgver:6:2}-${pkgver:8:2}T${pkgver:11:2}-${pkgver:13:2}-${pkgver:15:2}Z"
pkgrel=0
pkgdesc="The MinIO Client"
url="https://minio.io/"
arch="all"
license="AGPL-3.0-or-later"
makedepends="go"
source="https://github.com/minio/mc/archive/RELEASE.$_pkgver/minio-client-$_pkgver.tar.gz"
builddir="$srcdir/$_pkgname-RELEASE.$_pkgver"

# secfixes:
#   0.20230111.031416-r0:
#     - CVE-2022-41717

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -modcacherw"

build() {
	local prefix='github.com/minio/mc/cmd'
	local date=${_pkgver%%T*}
	local time=${_pkgver#*T}

	go build -tags kqueue -o bin/mcli -ldflags "
		-X $prefix.Version=${date}T${time//-/:}
		-X $prefix.CopyrightYear=${date%%-*}
		-X $prefix.ReleaseTag=RELEASE.$_pkgver
		-X $prefix.CommitID=0000000000000000000000000000000000000000
		-X $prefix.ShortCommitID=000000000000
		"
}

check() {
	# mc/cmd is disabled, seems to be outdated and fails on all my systems
	# shellcheck disable=2046
	go test -tags kqueue $(go list ./... | grep -v cmd)
}

package() {
	install -Dm755 bin/mcli -t "$pkgdir"/usr/bin/
}

sha512sums="
9992562a41926cebd1b3a72105cace1b65a6da932d745b246f134ede32da9e8d615a7674a4f58aafe4d538399269eb8a0fc673875c5e2e453ad7de64c83a331e  minio-client-2023-09-22T05-07-46Z.tar.gz
"
