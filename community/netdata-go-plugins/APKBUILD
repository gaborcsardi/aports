# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Maintainer: Henrik Riomar <henrik.riomar@gmail.com>
pkgname=netdata-go-plugins
pkgver=0.55.0
pkgrel=0
pkgdesc="netdata go.d.plugin"
url="https://github.com/netdata/go.d.plugin"
arch="all !x86 !armv7 !armhf" # checks fail
license="GPL-3.0-or-later"
depends="netdata"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://codeload.github.com/netdata/go.d.plugin/tar.gz/refs/tags/v$pkgver"
builddir="$srcdir/go.d.plugin-$pkgver"

export GOFLAGS="$GOFLAGS -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o go.d.plugin ./cmd/godplugin
}

check() {
	go test ./...
}

package() {
	 mkdir -p "$pkgdir/usr/lib/netdata/conf.d"
	 cp -r "$builddir/config/go.d.conf" "$builddir/config/go.d" "$pkgdir/usr/lib/netdata/conf.d/"

	 mkdir -p "$pkgdir/usr/libexec/netdata/plugins.d/"
	 install -D -m755 -t "$pkgdir/usr/libexec/netdata/plugins.d" "$builddir/go.d.plugin"
}

sha512sums="
707128fbb316d6f3320dc3faf9e81b5eb69dd036decabc5bfa99aa26e3be788996c97f4b2d098c6b842b69f858667d0cf61a8a0a55cef4cc72778e3c01f5b670  netdata-go-plugins-0.55.0.tar.gz
"
