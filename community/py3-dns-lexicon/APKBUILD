# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-dns-lexicon
pkgver=3.14.1
pkgrel=0
pkgdesc="Manipulate DNS records on various DNS providers in a standardized/agnostic way"
url="https://github.com/AnalogJ/lexicon"
arch="noarch"
license="MIT"
depends="
	py3-beautifulsoup4
	py3-cryptography
	py3-future
	py3-requests
	py3-tldextract
	py3-yaml
	"
makedepends="py3-gpep517 py3-poetry-core"
_providerdepends="
	py3-boto3
	py3-localzone
	py3-oci
	py3-softlayer
	py3-softlayer-zeep
	py3-xmltodict
	"
checkdepends="
	py3-filelock
	py3-pytest
	py3-pytest-mock
	py3-pytest-xdist
	py3-requests-file
	py3-vcrpy
	$_providerdepends
	"
subpackages="$pkgname-pyc"
source="$pkgname-github-$pkgver.tar.gz::https://github.com/AnalogJ/lexicon/archive/refs/tags/v$pkgver.tar.gz
	importlib.patch
	"
builddir="$srcdir/lexicon-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	# auto and namecheap tests fail in rootbld
	.testenv/bin/python3 -m pytest \
		--ignore tests/providers/test_localzone.py \
		--ignore tests/providers/test_auto.py \
		--ignore tests/providers/test_namecheap.py
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
	rm -rf "$pkgdir"/usr/lib/python3*/site-packages/lexicon/tests
}

sha512sums="
13e4c5b046cad28e487b246b7c16a3ee71b2dbca4c40e513b234cee62e483e7702ce293c11ef96b245409229a7cd3a39c1771ebfc7b2430cd5b017d33e32a924  py3-dns-lexicon-github-3.14.1.tar.gz
0273320a9a5bd6371efc30a24c1291c1160fb662d2015b8720d6a2ed044d562909124a20b5b7675d965888b506423f5203ac71b77f6ac2d4e52f283daa1187ac  importlib.patch
"
