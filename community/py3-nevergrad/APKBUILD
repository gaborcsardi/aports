# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-nevergrad
pkgver=0.13.0
pkgrel=1
pkgdesc="A Python toolbox for performing gradient-free optimization"
url="https://github.com/facebookresearch/nevergrad"
arch="noarch !s390x" #py3-bayesian-optimization
license="MIT"
depends="python3 py3-bayesian-optimization py3-cma py3-numpy py3-pandas py3-typing-extensions"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest-xdist py3-pytest-cov"
subpackages="$pkgname-pyc"
source="https://github.com/facebookresearch/nevergrad/archive/$pkgver/nevergrad-$pkgver.tar.gz"
builddir="$srcdir/nevergrad-$pkgver"
options="!check" # several test dependencies are missing | skip tests for now

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
810fc0d973a596c2d9d44b30966706b09e959232fd2af8f68a7fdd64f0fd31166f0c0b2b1310ccbe4aed085f3ee3e6b1db61fe4abdd53d3cbec190c7d7f69075  nevergrad-0.13.0.tar.gz
"
