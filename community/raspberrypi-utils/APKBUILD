# Contributor: macmpi <spam@ipik.org>
# Maintainer: macmpi <spam@ipik.org>
pkgname=raspberrypi-utils
pkgver=0.20230919
pkgrel=0
_commit="ddaafebe534ef5e2f2d9421e778752e685df44dd"
pkgdesc="Collection of Raspberry Pi utilities (scripts and simple applications)"
url="https://github.com/raspberrypi/utils"
arch="armhf armv7 aarch64"
license="BSD-3-Clause"
makedepends="cmake samurai dtc-dev"
source="$pkgname-$_commit.tar.gz::https://github.com/raspberrypi/utils/archive/$_commit.tar.gz"
builddir="$srcdir/utils-$_commit"
# does not have any tests
options="!check"
subpackages="
	$pkgname-vclog
	$pkgname-raspinfo::noarch
	$pkgname-dtmerge
	$pkgname-dtmerge-doc
	$pkgname-ovmerge::noarch
	$pkgname-overlaycheck::noarch
	$pkgname-otpset::noarch
	$pkgname-pinctrl
	"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_DATADIR=/usr/share \
		-DCMAKE_BUILD_TYPE=MinSizeRel
	cmake --build build
}

package() {
	depends="
		$pkgname-vclog=$pkgver-r$pkgrel
		$pkgname-raspinfo=$pkgver-r$pkgrel
		$pkgname-dtmerge=$pkgver-r$pkgrel
		$pkgname-ovmerge=$pkgver-r$pkgrel
		$pkgname-overlaycheck=$pkgver-r$pkgrel
		$pkgname-otpset=$pkgver-r$pkgrel
		$pkgname-pinctrl=$pkgver-r$pkgrel
		"
	DESTDIR="$pkgdir" cmake --install build
}

vclog() {
	pkgdesc="A tool to get VideoCore 'assert' or 'msg' logs with optional -f to wait for new logs to arrive."
	depends=""

	amove usr/bin/vclog
}

raspinfo() {
	pkgdesc="A short script to dump information about the Pi. Intended for the submission of bug reports."
	depends="
		$pkgname-vclog=$pkgver-r$pkgrel
		$pkgname-pinctrl=$pkgver-r$pkgrel
		bash
		raspberrypi-userland
		sudo-virt
		usbutils
		"
	# missing https://github.com/raspberrypi/rpi-eeprom

	amove usr/bin/raspinfo
}

dtmerge() {
	pkgdesc="A tool for applying compiled DT overlays (*.dtbo) to base Device Tree files (*.dtb)."
	depends=""

	amove usr/bin/dtmerge
}

ovmerge() {
	pkgdesc="A tool for merging DT overlay source files (*-overlay.dts), flattening and sorting .dts files for easy comparison, displaying the include tree, etc."
	depends="perl"

	amove usr/bin/ovmerge
}

overlaycheck() {
	pkgdesc="A tool for validating the overlay files and README in a kernel source tree."
	depends="
		$pkgname-dtmerge=$pkgver-r$pkgrel
		$pkgname-ovmerge=$pkgver-r$pkgrel
		dtc
		perl
		"

	amove usr/bin/overlaycheck
	amove usr/bin/overlaycheck_exclusions.txt
}

otpset() {
	pkgdesc="A short script to help with reading and setting the customer OTP bits."
	depends="python3 raspberrypi-userland"

	amove usr/bin/otpset
}

pinctrl() {
	pkgdesc="A tool for displaying and modifying the GPIO and pin muxing state of a system, bypassing the kernel."
	depends=""

	amove usr/bin/pinctrl
}

sha512sums="
5edf78237ada8ab68d5caa6e3bfbd394d20563ea70b8db48fc9b6f3feadd4662594d6c76bac7f464963e5e1557d7b24fb119cab385910ce3fe88eb08f171936f  raspberrypi-utils-ddaafebe534ef5e2f2d9421e778752e685df44dd.tar.gz
"
