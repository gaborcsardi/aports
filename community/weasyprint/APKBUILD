# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=weasyprint
pkgver=60.0
pkgrel=0
pkgdesc="A visual rendering engine for HTML and CSS that can export to PDF"
url="https://weasyprint.org"
arch="noarch"
license="BSD-3-Clause"
depends="
	pango
	py3-brotli
	py3-cssselect2
	py3-cffi
	py3-fonttools
	py3-html5lib
	py3-pillow
	py3-pydyf
	py3-pyphen
	py3-tinycss2
	py3-zopfli
	"
makedepends="
	py3-flit-core
	py3-gpep517
	"
checkdepends="
	font-dejavu
	ghostscript
	py3-pytest
	py3-pytest-cov
	py3-pytest-flake8
	py3-pytest-isort
	py3-pytest-xdist
	"
replaces="py-weasyprint py3-weasyprint"  # for backward compatibility
provides="py-weasyprint=$pkgver-r$pkgrel py3-weasyprint=$pkgver-r$pkgrel"  # for backward compatibility
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/w/weasyprint/weasyprint-$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest --deselect tests/draw/test_text.py::test_otb_font
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/weasyprint-$pkgver-py3-none-any.whl
}

sha512sums="
94abcd19a5798395ba8f9c5ddde616e877b359e5938f2c0247bb61550889af14aa00b1093ac547ecc94bb3a733ef5604daf251dad41a8418778ffb0aa561ab8a  weasyprint-60.0.tar.gz
"
