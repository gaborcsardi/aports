# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=mpg123
pkgver=1.32.1
pkgrel=0
pkgdesc="Console-based MPEG Audio Player for Layers 1, 2 and 3"
options="libtool"
url="https://www.mpg123.org"
arch="all"
license="LGPL-2.1-only"
subpackages="$pkgname-libs $pkgname-dev $pkgname-doc"
makedepends="libtool alsa-lib-dev linux-headers"
source="https://www.mpg123.org/download/mpg123-$pkgver.tar.bz2"

build() {
	CFLAGS="$CFLAGS -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-dependency-tracking \
		--with-pic \
		--with-optimization=0 \
		--with-cpu=i386_fpu \
		--with-audio="alsa oss"
	make
}

check() {
	make check
}

package() {
	# Installation is not parallel friendly and will fail
	# sometimes
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="
084f4575d3ad88373a04035778b40e4871b6da969f42b426c76d9539632baa12534d7f0f9b976be228fd313dea9c31f7a259e0a8b56d044c7e89fefdf897def2  mpg123-1.32.1.tar.bz2
"
