# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=lemmy
pkgver=0.18.4
# this needs to be synced with lemmy-ui
_translations_commit=b122306e52d94807528068a7e8f8011c29d31db1
pkgrel=0
pkgdesc="Link aggregator and forum for the Fediverse - Backend server"
url="https://join-lemmy.org/"
# armhf, armv7: fatal runtime error while compiling lemmy_server crate
# ppc64le, riscv64, s390x: ring crate
# x86: follow lemmy-ui aport
arch="aarch64 x86_64"
license="AGPL-3.0-only"
depends="postgresql-contrib" # pgcrypto
makedepends="
	cargo
	cargo-auditable
	libpq-dev
	openssl-dev
	"
install="$pkgname.pre-install $pkgname.post-install"
pkgusers="lemmy"
pkggroups="lemmy"
subpackages="$pkgname-openrc"
source="https://github.com/LemmyNet/lemmy/archive/$pkgver/lemmy-$pkgver.tar.gz
	https://github.com/LemmyNet/lemmy-translations/archive/$_translations_commit/lemmy-translations-$pkgver.tar.gz
	fix-db-init-script.patch
	lemmy.initd
	"

# html2md required by lemmy_apub fails to build with panic=abort
export CARGO_PROFILE_RELEASE_PANIC="unwind"

prepare() {
	default_prepare

	rmdir -v crates/utils/translations
	ln -sv "$srcdir/lemmy-translations-$_translations_commit" \
		crates/utils/translations

	sed -i "s/unknown version/$pkgver/" crates/utils/src/version.rs
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/lemmy_server -t "$pkgdir"/usr/bin
	install -Dm640 -g lemmy config/defaults.hjson \
		"$pkgdir"/etc/lemmy/lemmy.hjson

	install -Dm755 scripts/db-init.sh -t "$pkgdir"/usr/lib/$pkgname
	install -Dm755 "$srcdir"/lemmy.initd "$pkgdir"/etc/init.d/lemmy
}

sha512sums="
3e88e8fe8b3bf91cc1b0890408a74a0bb3d822d014b7d69cfe0156580f2e1f6c98a6c3fd743c722f0dcb3a1d86b0168b1e83c965ca39a33047e4fc8f8c88dcaf  lemmy-0.18.4.tar.gz
89c4bf3adc96b8a27996c09406658597266f9b16afcc5a62a5b221ef70556d6e02828a2a8de20392ef461a4e4867c5d9312cad01f1cd126b46a2d7e9693732f4  lemmy-translations-0.18.4.tar.gz
fcfb3881abf8ee671bb55e47c71fef8ef1328a04b3278fd9602478260b0de0bdb55b27c1f6127e9729a65e3fc05cbcd245bd1f98b93ea307755ff748a68d770d  fix-db-init-script.patch
38858d181c0cc3073c56963be1043b1da2d6ca2ae5683feead1154e345b29b0a73b3a86b17c29927749500240775c53bca92c28cb49ce9f5b778e4b4e8ebb87a  lemmy.initd
"
