# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-fastavro
_pkgname=fastavro
pkgver=1.8.3
pkgrel=0
pkgdesc="Fast Avro for Python"
# Tests for optional zstd and snappy codecs require
# unpackaged python modules 'zstandard' and 'python-snappy'
options="!check"
url="https://github.com/fastavro/fastavro"
arch="all !x86" # _tz_ tests fail
license="MIT"
depends="python3"
makedepends="py3-setuptools python3-dev cython"
checkdepends="py3-pytest py3-numpy"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/fastavro/fastavro/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	FASTAVRO_USE_CYTHON=1 python3 setup.py build
}

check() {
	PYTHONPATH="$(echo $PWD/build/lib.*)" python3 -m pytest -v tests
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
1b86a426da12d14cf883affbb4565118b241e3a317a92226eefa7ee1a6decd811f9256d569e7dc0e96507af664ad8624ca95eb23fb662a4ac2dbf345f0c1f62d  py3-fastavro-1.8.3.tar.gz
"
